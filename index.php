<?php 

$answers = ["Real Madrid", "Manchester United", "Chelsea", "Portugal", "Spain"];
$c = 1;
$data_invalid = null;
if(isset($_POST['submit'])) {
    if(count($_POST) < 6) {
        $data_invalid = "Please answer all questions";
    }
    else {
        $degree = 0;
        foreach($answers as $answer) {
            if($_POST['q'.$c] == $answer) {
                $degree++;
            }
            $c++;
        }
        
    }
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/paper.min.css">
        <title>Quiz</title>
        <style>
        body {
            background-color: #dfdfdf;
        }

        input {
            display: inline;
        }

        p {
            font-size: 25px;
        }
        </style>
    </head>

    <body>
        <div class="paper container border border-primary">
            <div class="row">
                <div class="col-4">
                    <h1>Quiz</h1>
                </div>
                <div class="col-4"></div>
                <?php if(isset($degree)) : ?>
                <div class="col-2 border border-4 border-primary" style="height: 220px; text-align: center;">
                    <h3><?= $degree ?></h3>
                    <div style="background-color: black; width: 50px; height: 3px; margin-left: 48px;"></div>
                    <h3>5</h3>
                </div>
                <?php endif; ?>
                <div class="col-2"></div>
            </div>

            <h1><?php $n ?></h1>
            <?php if(isset($data_invalid)) :?>
            <div class="row flex-spaces">
                <div class="alert alert-danger"><?= $data_invalid ?></div>
            </div>
            <?php endif ?>
            <form method="POST">
                <ol>
                    <li>
                        <p>Which team won UEFA Champions League in 2017?</p>
                        <div class="row">
                            <div class="col-4 col form-group">
                                <label for="a1" class="paper-radio">
                                    <input type="radio" name="q1" id="a1" value="Real Madrid"> <span>Real Madrid<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a2" class="paper-radio">
                                    <input type="radio" name="q1" id="a2" value="Barcelona"> <span>Barcelona<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a3" class="paper-radio">
                                    <input type="radio" name="q1" id="a3" value="Liverpool"> <span>Liverpool<span>
                                </label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>Which team won UEFA Champions League in 2008?</p>
                        <div class="row">
                            <div class="col-4 col form-group">
                                <label for="a2_1" class="paper-radio">
                                    <input type="radio" name="q2" id="a2_1" value="Chelsea"> <span>Chelsea<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a2_2" class="paper-radio">
                                    <input type="radio" name="q2" id="a2_2" value="Manchester United"> <span>Manchester
                                        United<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a2_3" class="paper-radio">
                                    <input type="radio" name="q2" id="a2_3" value="Liverpool"> <span>Liverpool<span>
                                </label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>Which team won UEFA Europa League in 2013?</p>
                        <div class="row">
                            <div class="col-4 col form-group">
                                <label for="a3_1" class="paper-radio">
                                    <input type="radio" name="q3" id="a3_1" value="Arsenal"> <span>Arsenal<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a3_2" class="paper-radio">
                                    <input type="radio" name="q3" id="a3_2" value="Chelsea"> <span>Chelsea<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a3_3" class="paper-radio">
                                    <input type="radio" name="q3" id="a3_3" value="Manchester United"> <span>Manchester
                                        United<span>
                                </label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>Which national team won UEFA Nations League in 2019?</p>
                        <div class="row">
                            <div class="col-4 col form-group">
                                <label for="a4_1" class="paper-radio">
                                    <input type="radio" name="q4" id="a4_1" value="France"> <span>France<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a4_2" class="paper-radio">
                                    <input type="radio" name="q4" id="a4_2" value="Germany"> <span>Germany<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a4_3" class="paper-radio">
                                    <input type="radio" name="q4" id="a4_3" value="Portugal"> <span>Portugal<span>
                                </label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>Which national team won Wrold Cup in 2010?</p>
                        <div class="row">
                            <div class="col-4 col form-group">
                                <label for="a5_1" class="paper-radio">
                                    <input type="radio" name="q5" id="a5_1" value="Germany"> <span>Germany<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a5_2" class="paper-radio">
                                    <input type="radio" name="q5" id="a5_2" value="Spain"> <span>Spain<span>
                                </label>
                            </div>
                            <div class="col-4 col form-group">
                                <label for="a5_3" class="paper-radio">
                                    <input type="radio" name="q1" id="a5_3" value="Italy"> <span>Italy<span>
                                </label>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="row flex-right">
                    <input type="submit" class="paper-btn btn-primary col-3" name="submit" value="Submit" />
                    <div class="col-5"></div>
                </div>
            </form>
        </div>
    </body>

</html>